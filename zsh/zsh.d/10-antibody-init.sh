#!/bin/bash
# load all plugins
if [ -x "$(command -v antibody)" ]; then
  . <(antibody init)
  antibody bundle < ~/zsh.d/.antibody_plugins
fi
