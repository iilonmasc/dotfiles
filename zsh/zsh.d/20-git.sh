#!/bin/bash
if [ -x "$(command -v git-cz)" ]; then
  alias gcz="git cz"
fi
if [ -x "$(command -v git)" ]; then
  alias gsac="git stash apply && git stash clear"
fi
