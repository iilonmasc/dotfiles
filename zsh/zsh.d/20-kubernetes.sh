#!/bin/bash
if [ -x "$(command -v kubectl)" ]; then
  alias k='kubectl'
  alias kg='kubectl get'
  alias kgj='kubectl get -o json'
  alias kgy='kubectl get -o yaml'
  alias kd='kubectl describe'
  alias ka='kubectl apply'
  alias kaf='kubectl apply -f'
  alias krm='kubectl delete'
  alias krp='kubectl replace'
  alias krpf='kubectl replace -f'
  alias kl='kubectl logs'
  alias klf='kubectl logs -f'
  alias kex='kubectl exec -it'
  alias ked='kubectl edit'
  alias ks='kubectl set'
  alias kcp='kubectl cp'
  alias ktn='kubectl top node'
  alias ktp='kubectl top pod'
fi
if [ -x "$(command -v kubectx)" ]; then
  alias kns='kubens'
fi
if [ -x "$(command -v kubens)" ]; then
  alias kcx='kubectx'
fi
