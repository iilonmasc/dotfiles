#!/bin/bash
if [ -x "$(command -v pacman)" ]; then
  alias pams="sudo pacman -S"
  alias syu="sudo pacman -Syu"
fi

if [ -x "$(command -v yay)" ]; then
  alias yays="yay -S"
  alias ysyu="yay -Syyu --answerdiff None --answerclean All --answeredit None --answerupgrade None --removemake --cleanafter"
fi
