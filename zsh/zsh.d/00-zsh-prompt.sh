#!/bin/bash

function +vi-git-st() {
local ahead behind remote
local -a gitstatus

# Are we on a remote-tracking branch?
remote=${$(git rev-parse --verify ${hook_com[branch]}@{upstream} \
  --symbolic-full-name 2>/dev/null)/refs\/remotes\/}

if [[ -n ${remote} ]] ; then
  ahead=$(git rev-list ${hook_com[branch]}@{upstream}..HEAD 2>/dev/null | wc -l)
  (( $ahead )) && gitstatus+=( "%F{112}+${ahead}%f" )

  behind=$(git rev-list HEAD..${hook_com[branch]}@{upstream} 2>/dev/null | wc -l)
  (( $behind )) && gitstatus+=( "%F{196}-${behind}%f" )

  if [[ -n ${gitstatus} ]] ; then
    hook_com[branch]="${hook_com[branch]} ${(j:/:)gitstatus}"
  else
    hook_com[branch]="${hook_com[branch]}"
  fi
fi
}
function current_k8s_context(){
  if [ -x "$(command -v kubectl)" ] && ( kubectl config current-context >/dev/null 2>&1 ); then
    echo -n " [$(kubectl config current-context)/$(kubectl config view --minify --output 'jsonpath={..namespace}')] "
  else
    echo -n " "
  fi
}

