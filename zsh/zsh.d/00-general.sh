#!/bin/bash
if [[ $commands[kubectl] ]]; then
  . <(kubectl completion zsh)
fi
if [ -x "$(command -v thefuck)" ]; then
  eval $(thefuck --alias)
fi

autoload -z edit-command-line
zle -N edit-command-line
bindkey "^X^E" edit-command-line
[ -f ~/.fzf.zsh ] && . ~/.fzf.zsh

if [ -x "$(command -v pacman)" ] && [ -x "$(pacman -Qq pkgfile)" ]; then
  source /usr/share/doc/pkgfile/command-not-found.zsh
fi
