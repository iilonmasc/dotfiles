#!/bin/bash
# if exa is installed, use exa instead of ls
if [ -x "$(command -v exa)" ]; then
  alias ll='exa -laahF --icons'
  alias l='exa -lh --icons'
  alias la='exa -laah --icons'
  alias ls='exa --icons'
  alias lsa='exa -lah --icons'
else
  alias l='ls -lah'
  alias la='ls -lAh'
  alias ls='ls -G'
  alias lsa='ls -lah'
  alias ll='ls -lispa'
fi
if [ -x "$(command -v fzf)" ]; then
  alias fzf="fzf --preview 'head -100 {}'"
fi
if [ -x "$(command -v tmux)" ]; then
  alias tmuxn='tmux new -s ${$(basename $(pwd))//./-}'
fi
if [ -x "$(command -v ranger)" ]; then
  alias dotedit='ranger ~/.dotfiles'
fi
if [ -x "$(command -v nvim)" ]; then
  alias vim="nvim"
fi
if [ -x "$(command -v xpanes)" ]; then
  alias xpanesc='xpanes -ssc'
fi
alias v="${EDITOR}"
alias e="${EDITOR}"
alias du="du -ach | sort -h"
alias psg="ps aux | grep -v grep | grep -i -e VSZ -e"
alias mkdir="mkdir -pv"
if [ -x "$(command -v newsboat)" ]; then
  alias nb="newsboat"
fi
if [ -x "$(command -v taskbook)" ]; then
  alias tb='taskbook'
  alias tbt='taskbook -t'
  alias tbd='taskbook -d'
  alias tbc='taskbook -c'
fi
