#!/bin/bash
if [ -x "$(command -v ranger)" ]; then
  function confedit(){
    ranger ~/.config/"${1}"
  }
fi
function uecho(){
  local unicode="\u${1}"
  echo "${unicode}"
}
function venv(){
  . ~/.venv/"${1}"/bin/activate
}
if [ -x "$(command -v asciinema)" ] && [ -x "$(command -v asciicast2gif)" ]; then
  function shellrec(){
    recording=${1}
    if [ -z ${1} ]; then
      name="${HOME}/.recordings/$(date +'%Y-%m-%d_%H-%M')"
    else
      name="${HOME}/.recordings/${recording}"
    fi
    asciinema rec ${name}.cast && asciicast2gif "${name}.cast" "${name}.gif"
  }
fi
