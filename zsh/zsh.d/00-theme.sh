#!/bin/bash

if [ -n "${ZSH_TERMINAL_THEME}" ] && [ -f ~/zsh.d/themes/${ZSH_TERMINAL_THEME}.sh ]; then
. ~/zsh.d/themes/${ZSH_TERMINAL_THEME}.sh
else
. ~/zsh.d/themes/default.sh
fi
