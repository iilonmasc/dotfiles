#!/bin/sh

available=""
missing=""
while read tool; do
  if [ -x "$(command -v ${tool})" ]; then
    available="${available}\n${tool}"
  elif [ -x "$(command -v pacman)" ] && [ -x "$(pacman -Qq ${tool} &> /dev/null)" ]; then
    available="${available}\n${tool}"
  else
    missing="${missing}\n${tool}"
  fi
done < dependencies.txt

# printf "\n--Available Packages--"
# printf "${available}"
printf "\n--Missing Packages--"
printf "${missing}\n"
