let file_list = split(globpath('~/.config/nvim/conf.d', '*.vim'), '\n')

for file in file_list
    execute 'source' fnameescape(file)
endfor
